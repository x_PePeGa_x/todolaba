FROM python:3.10-slim

COPY requirements.txt /requirements.txt
COPY app /app

ARG PROXY

RUN if [ -z "$PROXY" ] ; then \
      pip install --upgrade -r requirements.txt;\
    else \
      pip install --proxy ${PROXY} -r requirements.txt;\
    fi

WORKDIR "app"

CMD ["python3", "-m", "uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000", "--reload"]


